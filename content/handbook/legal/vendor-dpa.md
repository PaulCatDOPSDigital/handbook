---
aliases: /handbook/legal/Vendor-DPA
title: "Vendor Data Processing Addendum"
description:
---
[Vendor Data Processing Addendum](https://gitlab.com/gitlab-com/legal-and-compliance/-/raw/master/Vendor_Data_Processing_Addendum__7.18.24_.pdf)
