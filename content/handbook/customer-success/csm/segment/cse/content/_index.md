---
title: "CSE Content"
description: "Learn about the Customer Success Engineering content team, catalog, creation and contribution workflows."
aliases:
- /handbook/customer-success/csm/segment/scale/content
---

## <i class="fa-solid fa-door-open" style="color: #B197FC;"></i> Welcome to the Customer Success Engineering Content Handbook

## <i class="fa-solid fa-signs-post" style="color: #B197FC;"></i> QuickLinks

### Team Workflow

<!---
#### Issue Templates

- [<i class="far fa-edit"></i> New Content Issue](...)

--->

#### Issue Boards

- [<i class="far fa-clipboard"></i> Team Workflow](https://gitlab.com/gitlab-com/customer-success/customer-success-engineering/content/-/boards/7672022?group_by=epic)
- [<i class="far fa-clipboard"></i> Content by Milestone](https://gitlab.com/gitlab-com/customer-success/customer-success-engineering/content/-/boards/7672054?group_by=epic)
<!---
- [<i class="far fa-clipboard"></i> Content by Assignee](https://gitlab.com/groups/gitlab-com/-/boards/7577841?label_name[]=DA-Type%3A%3AContent&label_name[]=developer-advocacy)
- [<i class="far fa-clipboard"></i> Content by Types](https://gitlab.com/groups/gitlab-com/-/boards/7577822?label_name[]=DA-Type%3A%3AContent&label_name[]=developer-advocacy)
--->

### Roadmap

- [<i class="far fa-clipboard"></i> Customer Success Engineering Roadmap](https://gitlab.com/groups/gitlab-com/customer-success/customer-success-engineering/-/roadmap?state=opened&sort=START_DATE_ASC&layout=MONTHS&timeframe_range_type=CURRENT_YEAR&progress=COUNT&show_progress=true&show_milestones=true&milestones_type=SUBGROUP&show_labels=true)

### Team Resources

- [<i class="far fa-calendar-alt"></i> Webinar and Labs Calendar](https://university.gitlab.com/pages/gitlab-user-webinars)

### Want to work with the team?

- [<i class="far fa-edit"></i> Content Idea Template](https://gitlab.com/gitlab-com/customer-success/customer-success-engineering/scale-cse/-/issues/new?issuable_template=content-idea-template)
- [<i class="far fa-edit"></i> Raise Content Bug](https://gitlab.com/gitlab-com/customer-success/customer-success-engineering/content/-/issues/new?issuable_template=bug)

---

## <i class="fa-solid fa-map-location-dot" style="color: #B197FC;"></i> CSE Content Team Strategy

The main goals of the CSE Content team are to:

- Produce High-Quality, Customer-Aligned Content in support of Customer Sucess Engineering goals, aligned with market needs
- Effectively market all content
- Establish Clear Metrics for data driven analysis
- Foster Cross-Functional Collaboration

<!---
### OKRs

### Highspot

The team's content is distributed in [Highspot](https://gitlab.highspot.com/)...

### Product Adoption Initiatives

- [Title](Issue/Epic) (internal).
- [Title](Issue/Epic) (internal).
--->

## <i class="fa-solid fa-users" style="color: #B197FC;"></i></i> Team members and focus areas

We are members of the [Customer Success Engineering team](/handbook/customer-success/csm/segment/cse).

| Team member |  Responsibilities |
|-------------|-------------|
| [Nicole Esplin](/handbook/company/team/#nesplin) <br/> Content Strategist | Cross Functional Collaboration, Content Strategy, Marketing/Promotion, Content Optimization, Content Delivery |
| [Tearyne Almendariz](/handbook/company/team/#talmendariz) <br/> Content Architect | Quarterly Planning/Backlog Management, Content Creation (Non Demos), Content Operations |
| [James Wormwell](/handbook/company/team/#jwormwell) <br/> Demo Architect |  Content Creation (Demos), Demos/Lab Content Creation, Demo Operations, Demo Asset Maintenance |

> **Note**: Details articulating what constitutes `Responsibilities` can be found [here](https://docs.google.com/spreadsheets/d/1oa8pl4bESeObNVXqlWt3cR3DEOkRRAnIKWz7vzZwjuo/edit?gid=0#gid=0) (internal) and will continually be updated as the team evolves and matures.

## <i class="fa-solid fa-book" style="color: #B197FC;"></i> Content Catalog

The Content team creates content that will be maintained and can be reused for customers to consume.

You can search for relevant content and contact the team in the linked content projects or in the [#cse-content-team](https://gitlab.enterprise.slack.com/archives/C07EE4FNM9T) Slack channel.

The following sections provide an overview of all content assets, and links to find them.

### Environments/Infrastructure to support Demonstrating Content

- Build Your Own - Self service. GCP/AWS [Cloud Sandoxes](handbook/infrastructure-standards/realms/sandbox/#individual-aws-account-or-gcp-project)
- Self-Managed - Shared Omnibus instance [cs.gitlabdemo.cloud](https://cs.gitlabdemo.cloud) (internal) with visibility of Admin areas.
- SaaS - GitLab Licensed Demo Groups [Premium and Ultimate Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=GitlabCom_Licensed_Demo_Group_Request) (internal)
- [Produce effective demos with OBS Studio](https://docs.google.com/document/d/1kchnm55N8zx8tBBsxilWadGqBndhvb5d4eG9LsSS6DA/edit#heading=h.quzn6r2hna1l) (internal)

### Webinars

| Title                                                        | Group         | Last updated |
| ------------------------------------------------------------ | ------------- | ------------ |
| [Holistic Approach to Securing the Development Lifecycle](https://drive.google.com/file/d/11-mPw0aNXcazOMVVVvxEo97meQz1TYMW/view?usp=drive_link) | Secure        | 2023-10-13   |
| [Git Basics](https://drive.google.com/file/d/17BvOGiXmWNLYm3MXmIqZ6kAkC0b4cow7/view?usp=drive_link) | Create        | 2023-10-13   |
| [Intro to GitLab](https://drive.google.com/file/d/14vWu6oCIcWwrkNtcZw_pioC8K3c2hNEt/view?usp=drive_link) | All           | 2023-12-07   |
| [Intro to CI/CD](https://drive.google.com/file/d/1V3sH4rTQSMzFfwZpzZgmi9wZJq8vSoMm/view?usp=drive_link) | Verify        | 2023-10-13   |
| [Advanced CI/CD](https://drive.google.com/file/d/1GlGg0Q7p7gsAGGWgZ1vj82NZmap7PX3w/view?usp=drive_link) | Verify        | 2023-10-23   |
| [AI Powered DevSecOps](https://drive.google.com/file/d/1Y426FrNWLIFl3u40-yXBEdy-D_RM4TAO/view?usp=drive_link) | Data Science  | 2024-06-25   |
| [Getting Started with DevSecOps Metrics](https://drive.google.com/file/d/1YRBQzNyp1Fdb-kt_PUFk9fYWHpCR7gOz/view?usp=drive_link) | Plan          | 2023-10-23   |
| [Continuous Change Management in a Secure Way](https://drive.google.com/file/d/1ctwS4FpaEbrywn_ybZlsZDGLkfdYKe8G/view?usp=drive_link) | Secure        | 2023-10-23   |
| [Security and Compliance](https://drive.google.com/file/d/1UK56of57h-BVccZODI5awKTZUnpEq8fF/view?usp=drive_link) | Secure        | 2023-10-31   |
| [Jira to GitLab](https://drive.google.com/file/d/1ME_oU5zGtySPoAf8_I-3u5jJZW-kBMSo/view?usp=drive_link) | Verify        | 2023-12-18   |
| [GitLab Administration (SaaS)](https://drive.google.com/file/d/1JQYVed7StwOBGEnzmsT7yiDmDfNSkx_a/view?usp=drive_link) | Core Platform | 2023-10-23   |
| [GitLab Runners](https://drive.google.com/file/d/1nxglK5j8D5XsbZTaylN-HbbVJ0gKojJd/view?usp=drive_link) | Verify        | 2024-01-17   |
| [Vulnerability Management Strategies](https://drive.google.com/file/d/1DRhHsgeqRPGpu2NR5726QSGqg6bh7aJS/view?usp=drive_link) | Govern        | 2024-05-07   |
| [Separation of Duties](https://drive.google.com/file/d/16YcUdYDNPP0x0vXzG01OsCODVnYhYe4O/view?usp=drive_link) | Govern        | 2024-06-18   |
| [What's New! GitLab 17.0](https://drive.google.com/file/d/11EhjSsgMepd9iZYY9vNz8LFoQPLGVNuS/view?usp=drive_link) | All           | 2024-06-04   |
| [CI/CD Components](https://drive.google.com/file/d/1mSj3YhvTu5llgRzqRMZ0Lk08KlFLlhp4/view?usp=drive_link) | Create           | 2024-07-11   |
| [DAST API and Security Testing](https://drive.google.com/file/d/1G8XeiaQDpGQAyd1gwLsYmaf-tp3N4p91/view?usp=drive_link) | Secure           | 2024-07-12   |

> **Note**: Recordings are stored in the [Webinar Master Recordings folder](https://drive.google.com/drive/folders/1x0_7J30cTpfbRXjrXgG_2XOIARLusNt3?usp=drive_link) (internal)

### Labs

| Title                                                        | Project                                                      | Group          | Last updated |
| ------------------------------------------------------------ | ------------------------------------------------------------ | -------------- | ------------ |
| [AI in DevSecOps](https://docs.google.com/presentation/d/1GdS0MQI53_mxQG-VvPxK2g9AmeJDmm403vSl8zZvy7I/edit?usp=drive_link) | [AI in DevSecOps](https://gitlab.com/gitlab-learn-labs/onboarding-cohort-projects/ai-in-dev-sec-ops/) | Data Science   | 2024-04-30   |
| [GitLab CI](https://docs.google.com/presentation/d/1IiRo4KHAgYqmzNiLkNYEatzHo75ax1BNKy-HsHoZW3k/edit?usp=drive_link) | [CICD Adoption Workshop](https://gitlab.com/gitlab-learn-labs/sample-projects/cicd-adoption-workshop) | Verify         | 2023-10-23   |
| [GitLab Advanced CI](https://docs.google.com/presentation/d/1g36th6wlPUj9YMHooAr7M0koscEdDAnxJULTu3F93Fg/edit?usp=drive_link) | [Advanced CI Lab](https://gitlab.com/gitlab-learn-labs/onboarding-cohort-projects/advanced-ci-lab/-/tree/main?ref_type=heads) | Package/Verify | 2024-05-10   |
| [CI/CD Adoption for Jenkins Users](https://docs.google.com/presentation/d/1d2u6Ls_ELgEAv8VXMatljVkPydOelUQ3_hsOvUe2k28/edit?usp=drive_link) | [CICD Adoption Workshop](https://gitlab.com/gitlab-learn-labs/sample-projects/cicd-adoption-workshop) | Verify         | 2024-01-10   |
| [Security and Compliance](https://docs.google.com/presentation/d/1_o1UbmM0u96f9XTpjYBLG3jnHeJuwJAVOrCg7Ri4ti4/edit#slide=id.g2e71b1d1f20_1_598) | [Tanuki Racing Security and Compliance](https://gitlab.com/gitlab-learn-labs/onboarding-cohort-projects/tanuki-racing-security-and-compliance) | Govern/Secure  | 2024-06-26   |

## <i class="fa-solid fa-folder-plus" style="color: #B197FC;"></i> Content Creation Process

The CSE Content team creates, promotes, and distributes content that is focused on customer enablement and adoption of GitLab features, tools, and best practices. The types of content we currently create are **Short Form Demos, Labs, and Webinars**. This content is initially designed for the 1:many audience use by our Customer Success Engineers, but we aspire for the content to be used and adapted to 1:1 customer engagements and other uses within GitLab.

### Suggesting Content

We are always taking suggestions for new content to enable customers. Please visit our [content idea backlog here](https://gitlab.com/gitlab-com/customer-success/customer-success-engineering/scale-cse/-/boards/7694684) to see if a topic already exists; and you also can [submit an idea for content here using our ideation template](https://gitlab.com/gitlab-com/customer-success/customer-success-engineering/scale-cse/-/issues/new?issuable_template=content-idea-template) if the content piece that you have in mind has not already been suggested.

Submissions will be reviewed and refined on a quarterly basis during our backlog review, facilitated by the team's Content Architect. If an idea is chosen for  creation, it will be promoted to an epic in the Customer Success Engineering Group and assigned issues for the creation process associated with that content type. While this is a collaborative process, the Director of the Customer Success Engineering team has the final say on which content will be created each quarter.

While our goal is to establish a more structured process that we can measure to ensure it delivers value to our customers, CSEs are encouraged to research topics as needed for customer needs or personal growth. CSEs are asked to please discuss any content projects outside of the official CSE Content Team process and workflow directly with their managers. We are working toward a set of quality assurance guidelines and standards which one can use to independently evaluate personal projects that they may want to propose be adapted to a future webinar or lab. 
