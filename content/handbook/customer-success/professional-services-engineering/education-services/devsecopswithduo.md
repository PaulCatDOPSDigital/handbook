---
title: "DevSecOps with GitLab Duo - Hands-On Lab Overview"
description: "This Hands-On Guide walks you through the lab exercises in the DevSecOps with GitLab Duo course."
---

## DevSecOps with GitLab Duo

| Lab Name | Lab Link |
|-----------|------------|
| Getting Started with GitLab Duo | [Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/devsecopswithduolab1) |
| Code Generation with GitLab Duo | [Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/devsecopswithduolab2) |
| Working with Issues and Merge Requests | [Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/devsecopswithduolab3) |
| Using GitLab Duo to Write New Code | [Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/devsecopswithduolab4) |
| Working with Security Vulnerabilities | [Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/devsecopswithduolab5) |

## Quick links

* DevSecOps with GitLab Duo Course Description

## Suggestions?

If you'd like to suggest changes to the *DevSecOps With GitLab Duo*, please submit them via merge request.
