---
title: Foundations Stage
description: The Foundations stage deals with cross functional work from the design system, to importers, to the navigation.
---

## About

We're the Foundations stage!
We're new here, and are building out this page so there's not much to see here.
In the mean-time, check out our groups' pages below.

## Teams

- [Foundations:Design System](design-system)
- [Foundations:Import and Integrate](import-and-integrate)
- [Foundations:Personal Productivity](personal-productivity)

## Metrics

{{< tableau height="600px" toolbar="hidden" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/TopEngineeringMetrics/TopEngineeringMetricsDashboard" >}}
  {{< tableau/filters "STAGE_LABEL"="foundations" >}}
{{< /tableau >}}

{{< tableau height="600px" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/MergeRequestMetrics/OverallMRsbyType_1" >}}
  {{< tableau/filters "STAGE_LABEL"="foundations" >}}
{{< /tableau >}}
