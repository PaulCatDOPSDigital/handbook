---
title: Manage Stage
---

## Manage

The responsibilities of this stage are described by the [Manage product category](/handbook/product/categories/#manage-stage).
Manage is made up of multiple groups, each with their own features and areas of responsibility.

* I have a question. Who do I ask?

In GitLab issues, questions should start by @ mentioning the relevant Product Manager for the [product category](/handbook/product/categories/#manage-stage).

GitLab team members can also use [#s_manage](https://gitlab.slack.com/messages/CBFCUM0RX).

## How we work

* In accordance with our [GitLab values](/handbook/values/).
* Transparently: nearly everything is public, we record/livestream meetings whenever possible.
* We get a chance to work on the things we want to work on.
* Everyone can contribute; no silos.
  * The goal is to have product give engineering and design the opportunity to be involved with direction and issue definition from the very beginning.

## OKRs

For a list of active OKRs, [check the OKR project](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?sort=created_date&state=opened&label_name%5B%5D=devops%3A%3Amanage&milestone_title=Started&first_page_size=50).

## Direction

The direction and strategy for Manage is documented on [https://about.gitlab.com/direction/manage/](https://about.gitlab.com/direction/manage/). This page (and the category direction
pages under the "Categories" header) is the single source of truth on where we're going and why.

* Direction pages should be reviewed regularly by Product. When updating these pages, please CC the relevant group to keep your teammates informed.
* Product should make sure that their groups understand the direction and have an opportunity to contribute to it. Consider a monthly direction AMA for your group to field questions.

## Career Development

{{% include "includes/engineering/manage-data-science-shared/career-development.md" %}}

## Team Days

{{% include "includes/engineering/manage-data-science-shared/team-day.md" %}}

## Holiday Gift Exchange (2023)

{{% include "includes/engineering/manage-data-science-shared/holiday-exchange.md" %}}

## Say/Do Ratio

{{% include "includes/engineering/manage-data-science-shared/say-do-ratio.md" %}}

## Meetings

Although we have a bias for asynchronous communication, synchronous meetings are necessary and should adhere to our [communication guidelines](/handbook/communication/#video-calls). Some regular meetings that take place in Manage are:

| Frequency | Meeting                              | DRI         | Possible topics                                                                                        |
|-----------|--------------------------------------|-------------|--------------------------------------------------------------------------------------------------------|
| Every other Thursday | Engineering managers discussion | @m_gill | Ideas, help or resources needed from others, concerns, questions, etc.                                 |

For one-off, topic specific meetings, please always consider recording these calls and sharing them (or taking notes in a [publicly available document](https://docs.google.com/document/d/1kE8udlwjAiMjZW4p1yARUPNmBgHYReK4Ks5xOJW6Tdw/edit)).

Agenda documents and recordings can be placed in the [shared Google drive](https://drive.google.com/drive/u/0/folders/0ALpc3GhrDkKwUk9PVA) (internal only) as a single source of truth.

All meetings should have an agenda prepared at least 12 hours in advance. If this is not the case, you are not obligated to attend the meeting. Consider meetings canceled if they do not have an agenda by the start time of the meeting.

## Shared calendars

1. Manage: Important Dates - Use this calendar for stage-wide reminders (OKR due dates, promotion cycles, talent assessment timelines, regular holiday preparations)
   * To add this calendar to your Google Calendar, use this calendar ID `c_kvfbp2t8edtgm4rjialus0834g@group.calendar.google.com`
1. Manage Shared - Use this calendar for recurring group level reminders and meetings (Release kickoff, milestone planning, social calls)
   * To add the shared calendar to your Google Calendar, please use this [link](https://calendar.google.com/calendar/b/1?cid=Z2l0bGFiLmNvbV9rOWYyN2lqamExaGoxNzZvbmNuMWU4cXF2a0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) (GitLab internal).
   * To add a meeting to the shared calendar, please add [the link in this document](https://docs.google.com/document/d/1IxGuORI-vfVd6irNdUwpnOBZDWALWzOqhQzC9E39ixQ/edit) to the event.

* To add a new member to the shared calendar
  * Click "Settings and Sharing" in the kebab menu when mousing over "Manage Shared" in your Google Calendar sidebar under "My calendars".
  * Scroll to the "Share with specific people" section of the settings area. Click "Add people" and add the new member with "Make changes and manage sharing".
* For a more detailed walkthrough, have a look at a quick [video walkthrough](https://www.youtube.com/watch?v=TmcPuuljf1w)

## Dashboards

* [Performance indicator overview](/handbook/engineering/metrics/dev/manage/)

## Links and resources

{{% include "includes/engineering/manage/shared-links.md" %}}
{{% include "includes/engineering/manage-data-science-shared/shared-links.md" %}}

* Our Slack channels
  * Manage:Foundations [#g_manage_foundations](https://gitlab.slack.com/messages/C010NAWPRV4)
  * Manage:Import and Integrate [#g_manage_import_and_integrate](https://gitlab.slack.com/archives/C04RDL3MEH5)
* Issue boards
  * Foundations [current milestone work board](https://gitlab.com/groups/gitlab-org/-/boards/3871464?label_name%5B%5D=group%3A%3Afoundations&milestone_title=Started)
  * Import and Integrate [current milestone work board](https://gitlab.com/groups/gitlab-org/-/boards/1459244?milestone_title=Upcoming&label_name[]=group%3A%3Aimport%20and%20integrate)
